#!/bin/bash
set -o nounset
set -o errexit

memlimit=8388608 # 8GB
#memlimit=1048576 # 1GB
#memlimit=4194304 # 4GB

bindir=`dirname $0`
bindir=`readlink -f ${bindir}`
datadir="${bindir}/../data"
[ ! -d ${datadir} ] && mkdir ${datadir}
environment=${1:-local}
db=${datadir}/${environment}.rrd

case "$environment" in 
  local)
    memlimit=8388608 # 8GB
    ;;
  staging)
    memlimit=4194304 # 4GB
    ;;
  stagingapi)
    memlimit=4194304 # 4GB
    ;;
  prodapi)
    memlimit=4194304 # 4GB
    ;;
  dev)
    memlimit=1048576 # 1GB
    ;;
  demo)
    memlimit=1048576 # 1GB
    ;;
  linodeffm)
    memlimit=2097152 # 2GB
    ;;
  *)
    echo "Generic environment $environment, setting memlimit to $memlimit"
#    echo "Environment missing"
#    exit 1
esac

rrdtool create $db \
--step '10' \
--start '1525132800' \
'DS:cpu:GAUGE:20:0:100' \
"DS:mem:GAUGE:20:0:${memlimit}" \
'DS:swap:GAUGE:20:0:5242880' \
"DS:active:GAUGE:20:0:${memlimit}" \
"DS:unreclaim:GAUGE:20:0:${memlimit}" \
"DS:watts:GAUGE:20:0:50" \
"DS:battery:GAUGE:20:0:100" \
"DS:rxrate:COUNTER:20:U:U" \
"DS:txrate:COUNTER:20:U:U" \
"DS:rx:GAUGE:20:U:U" \
"DS:tx:GAUGE:20:U:U" \
'RRA:AVERAGE:0.5:30:8640' \
'RRA:AVERAGE:0.5:1:8640' \
'RRA:AVERAGE:0.5:6:1440' \
'RRA:AVERAGE:0.5:360:24' \
'RRA:MAX:0.5:30:8640' \
'RRA:MAX:0.5:1:8640' \
'RRA:MAX:0.5:6:1440' \
'RRA:MAX:0.5:360:24' 

# created with
#http://rrdwizard.appspot.com/
