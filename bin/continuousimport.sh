#!/bin/bash
set -o nounset
set -o errexit

bindir=`dirname $0`
bindir=`readlink -f ${bindir}`
environment=${1:-local}
cd ${bindir}

while true ; do ./import.sh ${environment} ../collect/allstats.txt  && ./create-graph.sh ${environment} ; sleep 30 ; done
