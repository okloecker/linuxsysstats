#!/bin/bash
set -o nounset
set -o errexit

bindir=`dirname $0`
bindir=`readlink -f ${bindir}`
datadir="${bindir}/../data"
environment=${1:-local}
db=${datadir}/${environment}.rrd
data=${datadir}/${environment}.txt

[ ! -f ${db} ] && $bindir/create.sh ${environment}

# filter out lines containing NUL characters
grep --binary-files=text -Pv '\x00' ${data} | split -l 100000 - ${data}.split.

# these come from:
# interface=wlp2s0

# Alternative 1
# awk "/${interface}/ {print \$2, \$10}" /proc/net/dev 
# And are used to get rx/tx values relative to startOfDay or any other baseline
# set -- $(awk "/wl/ {print \$2, \$10}" /proc/net/dev)
# rxbase=$1 
# txbase=$2

# Alternative 2
# rxbase=$(awk "/wl/ {print \$2}" /proc/net/dev) #1078461818 
# txbase=$(awk "/wl/ {print \$10}" /proc/net/dev) #53510299
# echo rxbase=${rxbase}
# echo txbase=${txbase}

# Alternative 3
rxbase=2294767498 
txbase=310588245
echo rxbase=${rxbase}
echo txbase=${txbase}



# stdin:
for splitdata in ${data}.split.* ; do
  echo processing $splitdata
  # '$rxbase' is temporarily exiting single quote environment so bash variable
  # can be read
  acc=`grep -v "^#" $splitdata | awk 'function gtzero(x){return (x < 0) ? 0 : x;} BEGIN {ORS=" "} {print $1":"$2":"$4":"$6":"$7":"$8":"$9":"$10":"$11":"$12":"gtzero($11-'$rxbase')":"gtzero($12-'$txbase') }'`
  echo "update --skip-past-updates $db $acc" | rrdtool -
done
rm ${data}.split.*
#echo "update $db $acc" | rrdtool -
