#!/bin/bash
set -o nounset
set -o errexit

bindir=`dirname $0`
target=${1:-local}
#${bindir}/create.sh $target && ${bindir}/import.sh $target ${bindir}/collect/${target}-allstats.txt && ${bindir}/create-graph.sh $target
echo "CREATE" && ${bindir}/create.sh $target && echo "IMPORT" && ${bindir}/import.sh $target ${bindir}/collect/${target}.txt && echo "CREATE GRAPH" && ${bindir}/create-graph2.sh $target
