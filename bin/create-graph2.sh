#!/bin/bash
set -o nounset
set -o errexit

# create names of images sorted by name-time-features
# instead of name-feature-time

bindir=`dirname $0`
bindir=`readlink -f ${bindir}`
datadir="${bindir}/../data"
environment=${1:-local}
db=${datadir}/${environment}.rrd
graphsdir=${bindir}/../graphs

[ ! -d ${graphsdir} ] && mkdir -p ${graphsdir}

case "$environment" in 
  prod)
    TZ=EST
    ;;
  prodapi)
    TZ=EST
    ;;
  staging)
    TZ=EST
    ;;
  stagingapi)
    TZ=EST
    ;;
  demo)
    TZ=EST
    ;;
  dev)
    TZ=EST
    ;;
  *)
    echo "Generic environment $environment, not setting TZ"
esac
export TZ

for time in 30d 10d 2d 1d 6h 30m ; do
  rrdtool graph "${graphsdir}/${environment}-${time}.cpu.png" \
  --width '1600' \
  --height '300' \
  --start end-${time} \
  --lower-limit '0' \
  --upper-limit '100' \
  "DEF:cpu=${db}:cpu:AVERAGE" \
  'LINE1:cpu#FF0000' 

  rrdtool graph "${graphsdir}/${environment}-${time}.mem.png" \
  --width '1600' \
  --height '300' \
  --start end-${time} \
  --lower-limit '0' \
  "DEF:mem=${db}:mem:AVERAGE" \
  "DEF:active=${db}:active:AVERAGE" \
  "DEF:swap=${db}:swap:AVERAGE" \
  "DEF:unreclaim=${db}:unreclaim:AVERAGE" \
  'LINE1:mem#FF0000':"mem" \
  'LINE1:active#00FF00':"active" \
  'LINE1:unreclaim#000000':"unreclaimed" \
  'LINE1:swap#0000FF':"swap"

  rrdtool graph "${graphsdir}/${environment}-${time}.net.png" \
  --width '1600' \
  --height '300' \
  --start end-${time} \
  --lower-limit '0' \
  "DEF:tx=${db}:tx:MAX" \
  "DEF:rx=${db}:rx:MAX" \
  'LINE1:tx#FF0000':"tx" \
  'LINE1:rx#00FF00':"rx" 

  rrdtool graph "${graphsdir}/${environment}-${time}.netrate.png" \
  --width '1600' \
  --height '300' \
  --start end-${time} \
  --lower-limit '0' \
  "DEF:txrate=${db}:txrate:AVERAGE" \
  "DEF:rxrate=${db}:rxrate:AVERAGE" \
  'LINE1:txrate#FF0000':"txrate" \
  'LINE1:rxrate#00FF00':"rxrate" 

  if [ ${environment} = 'local1' ] ; then
    rrdtool graph "${graphsdir}/${environment}-${time}.watts.png" \
    --width '1600' \
    --height '300' \
    --start end-${time} \
    --lower-limit '0' \
    --upper-limit '50' \
    "DEF:watts=${db}:watts:AVERAGE" \
    'LINE1:watts#FF0000' \
    "DEF:battery=${db}:battery:AVERAGE" \
    'LINE1:battery#00FF00' 
  fi
done

exit

