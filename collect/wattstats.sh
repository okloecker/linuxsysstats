#!/bin/bash
set -o errexit
set -o nounset

# prints power in watts

if [ -f /sys/class/power_supply/BAT0/power_now ] ; then
  awk '{print $1*10^-6}' /sys/class/power_supply/BAT0/power_now
else
  echo 0
fi

