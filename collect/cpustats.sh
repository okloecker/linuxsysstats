#!/bin/bash
set -o errexit
set -o nounset

# prints CPU idle percentage

LC_ALL=C top -bn2 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1}' | tail -1

