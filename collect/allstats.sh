#!/bin/bash
set -o errexit
set -o nounset

bindir=`dirname $0`
bindir=`readlink -f ${bindir}`
datadir="${bindir}/../data"
[ ! -d ${datadir} ] && mkdir ${datadir}
environment=${1:-local}
data=${datadir}/${environment}.txt
cd ${bindir}

echo "#starting logging at `date`" >> ${data}
while true ; do
  mem=`./memstats.sh`
  cpu=`./cpustats.sh`
  watts=`./wattstats.sh`
  battery=`./battstats.sh`
  network=`./netstats.sh`

  echo "`date +%s` $cpu $mem $watts $battery $network" >> ${data}

  sleep 10
done


