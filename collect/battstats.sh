#!/bin/bash
set -o errexit
set -o nounset

# prints battery level in percentage

#full=`cat /sys/class/power_supply/BAT0/energy_full`
#now=`cat /sys/class/power_supply/BAT0/energy_now`
#echo "${now}*100/${full}"  |bc
if [ -f /sys/class/power_supply/BAT0/capacity ] ; then
  cat /sys/class/power_supply/BAT0/capacity
else
  echo 0
fi
