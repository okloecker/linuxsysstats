#!/bin/bash
set -o errexit
set -o nounset

# prints "TotalMem UsedMem TotalSwap FreeSwap Active"

#awk '/MemTotal/{t=$2}/MemAvailable/{a=$2}/Active/{act=$2}/SwapTotal/{st=$2}/SwapFree/{sf=$2}END{print t" "t-a" "st" "st-sf" "act}' /proc/meminfo
#awk '/MemTotal/{t=$2}/MemFree/{f=$2}/Active/{act=$2}/SwapTotal/{st=$2}/SwapFree/{sf=$2}/SUnreclaim/{ur=$2}END{print t" "t-f" "st" "st-sf" "act" "ur}' /proc/meminfo

# MemAvailable instead of MemFree
awk '/MemTotal/{t=$2}/MemAvailable/{f=$2}/Active/{act=$2}/SwapTotal/{st=$2}/SwapFree/{sf=$2}/SUnreclaim/{ur=$2}END{print t" "t-f" "st" "st-sf" "act" "ur}' /proc/meminfo

